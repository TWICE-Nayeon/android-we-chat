package com.example.wechat;

import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;

public class weixinFragment extends Fragment {
    private RecyclerView recyclerView;
    private List<String> mList = new ArrayList<>();
    private Context context;
    private VerticalAdappter adapter;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    public weixinFragment() {

    }

    public static weixinFragment newInstance(String param1, String param2) {
        weixinFragment fragment = new weixinFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.tab01, container, false);
        recyclerView=view.findViewById(R.id.verticalview);
        initData();
        initView();

        return view;
    }

    private void initData() {
        mList.add("张纪媛");
        mList.add("林娜琏");
        mList.add("俞定延");
        mList.add("MOMO酱");
        mList.add("SANA酱");
        mList.add("朴志效");
        mList.add("MINA酱");
        mList.add("金多贤");
        mList.add("孙彩瑛");
        mList.add("周子瑜");
        mList.add("TWICE");
        mList.add("ONCE");
        mList.add("恰拉架");
        mList.add("FANCY");
        mList.add("LIKEY");
        mList.add("TT");
    }
    private void initView() {

        context=this.getActivity();
        adapter=new VerticalAdappter(context,mList);


        LinearLayoutManager manager=new LinearLayoutManager(context);
        manager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);
    }
}
